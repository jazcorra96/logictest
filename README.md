## Descripción
Este proyecto consiste en la solución de una prueba de algoritmia realizada en lenguaje Java. El problema consiste en lo siguiente:

>Starting at the top left corner of an N x M grid and facing towards the right, you keep walking one square at a time in the direction you are facing. If you reach the boundary of the grid or if the next square you are about to visit has already been visited, you turn right. You stop when all the squares in the grid have been visited. What direction will you be facing when you stop? For example: Consider the case with N = 3, M = 3. The path followed will be (0,0) -> (0,1) -> (0,2) -> (1,2) -> (2,2) -> (2,1) -> (2,0) -> (1,0) -> (1,1). At this point, all squares have been visited, and you are facing right.
>
>Input specification
The first line contains T the number of test cases. Each of the next T lines contain two integers N and M, denoting the number of rows and columns respectively.
>
>Output specification
Output T lines, one for each test case, containing the required direction you will be facing at the end. Output L for left, R for right, U for up, and D for down. 1 <= T <= 5000, 1 <= N,M <= 10^9.

---
## Solución
El problema plantea las siguientes condiciones
 
1. Inicia en la esquina superior izquierda
2. Inicia mirando hacia la derecha (R)
3. Avanza hacia donde mire
4. Al alcanzar un límite, y haber celda disponible, se voltea a la derecha y avanza
5. 1 < T < 5000
6. 1 < N,M < 10^9
	  
 La solución en la que pensé se basa en el hecho de que cada giro se da por pares (excepto en casos unitarios): cuando
 existen filas y columnas *disponibles*, cada vez que se finaliza una fila las columnas
 reducen en tamaño y análogamente las filas reducen en tamaño cuando se finaliza una columna.
 
 De tal manera que el número de giros que se tienen que efectuar está directamente relacionado con dos cosas:
 
 1. La orientación del grid (M<N , M>N , M=N)
 2. La paridad del lado menor ya que será el primero en reducirse a 0
 
 Así que el algoritmo consiste en los siguientes pasos:

 0. Determinar si es un caso especial (N=1 o M=1) y resolver
 1. Determinar la orientación del grid: Horizontal, Vertical o Cuadrado
 2. Determinar la paridad de fila
 3. Determinar la paridad de columna
 4. En función de la orientación verificar la paridad del lado más corto (row o column) y resolver

---
## Acerca del proyecto
El proyecto fue creado en lenguaje Java utilizando el IDE IntelliJ IDEA.
Para ejecutarlo con el mismo entorno se siguen los siguientes pasos:
1. Abrir la carpeta del proyecto en IntelliJ IDEA
2. Menu Run
3. Run 'Main'
4. Finalmente la solución se ejecutará en la consola del IDE

Otra forma de abrir la solución es ejecutando el archivo **StartLogicTest.cmd** con el archivo LogicTest.jar en la misma carpeta.