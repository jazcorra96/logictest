package mx.dacodes;

import java.util.Scanner;

/**
 * El problema plantea las siguientes condiciones
 *      - Inicia en la esquina superior izquierda
 *      - Inicia mirando hacia la derecha (R)
 *      - Avanza hacia donde mire
 *      - Al alcanzar un límite, y haber celda disponible, se voltea a la derecha y avanza
 *      -   1 < T < 5000
 *      -   1 < N,M < 10^9
 * La solución en la que pensé se basa en el hecho de que cada giro se da por pares (excepto en casos unitarios): cuando
 * existen filas y columnas "disponibles", cada vez que se finaliza una fila las columnas
 * reducen en tamaño y análogamente las filas reducen en tamaño cuando se finaliza una columna.
 * De tal manera que el número de giros que se tienen que efectuar está directamente relacionado con dos cosas:
 * - La orientación del grid (M<N , M>N , M=N)
 * - La paridad del lado menor ya que será el primero en reducirse a 0
 * Así que el algoritmo consiste en los siguientes pasos:
 *
 * 0- Determinar si es un caso especial (N=1 o M=1) y resolver
 * 1- Determinar la orientación del grid: Horizontal, Vertical o Cuadrado
 * 2- Determinar la paridad de fila
 * 3- Determinar la paridad de columna
 * 4- En función de la orientación verificar la paridad del lado más corto (row o column) y resolver
 *
 */
public class Main {
    enum Orientation{
        horizontal, vertical, square
    }
    enum Parity{
        even, odd
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Orientation orientation;
        Parity rowParity;
        Parity columnParity;
        int T = 0;
        int N = 0;
        int M = 0;
        while (!(0<T && T<=5000)) {
            System.out.print("How many test cases? T = ");
            T = sc.nextInt();
        }

        for(int i=0; i<T ; i++){
            System.out.println("-->Iteration T = " + i + "<--");
            while(!(0<N && N<=1000000000)||!(0<M && M<=1000000000)) {
                System.out.print("Rows = ");
                N = sc.nextInt();
                System.out.print("Columns = ");
                M = sc.nextInt();
            }

            if (N==1){
                System.out.println("Facing: RIGHT");
            }else if(M==1){
                System.out.println("Facing: DOWN");
            }else{
                //Check orientation
                orientation = (M==N) ? Orientation.square : (M>N? Orientation.horizontal : Orientation.vertical);
                rowParity = (N%2)==0? Parity.even : Parity.odd;
                columnParity = (M%2)==0? Parity.even : Parity.odd;
                if(orientation == Orientation.square){
                    if(rowParity == Parity.even){
                        System.out.println("Facing: LEFT");
                    }else{
                        System.out.println("Facing: RIGHT");
                    }
                }else if(orientation == Orientation.vertical) {
                    if (columnParity == Parity.even){
                        System.out.println("Facing: UP");
                    }else{
                        System.out.println("Facing: DOWN");
                    }
                }else if(orientation == Orientation.horizontal){
                    if(rowParity == Parity.even){
                        System.out.println("Facing: LEFT");
                    }else {
                        System.out.println("Facing: RIGHT");
                    }
                }
            }
            System.out.println();
            N = M = 0;

        }

        System.out.print("Press [Enter] To Exit...");
        sc.nextLine();
        sc.nextLine();
    }
}
